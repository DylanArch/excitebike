﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Follow : MonoBehaviour
{


    //Code by: Levi Roberts
    //Have Questions? Contact me @ LJRoberts@student.fullsail.edu 
    //or on campus any time at fullsail



    public Transform TargetToFollow; // for the MainCamera this should follow UI_Time_TEMP // for tempbar this the Player_Character. 
    public Transform SelfTrans; //for the MainCamera this is the MainCamera. //for the UI_Time_TEMP this is Player_Character. 
    public Vector3 offset = new Vector3(0f, 0f, 0f); //just set this to whatever you want in the inspector.


    private void LateUpdate() //Update late not instantly.
    {
        SelfTrans.position = new Vector3(TargetToFollow.position.x, 0, 0) + offset; // Pretty much just follows on the X axis plus the offset from its target.
    }
}
