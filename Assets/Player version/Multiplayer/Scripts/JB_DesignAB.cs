﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;


public class JB_DesignAB : MonoBehaviour

    // Code By Joshua Barber
{
   private MenuStuff source;
    private Dictionary<MenuState, Action> menu = new Dictionary<MenuState, Action>();
    //private MenuState curState = MenuState.SELECT1;
    private bool SoloOn; //Toggle Solo/AI Race
    public float time;
    private bool TitleDone = false; //Check for Title Music Playing
    public AudioSource audiosource; //All Sounds use this

    public GameObject menuImage;

   
    public string Mutliplayer_Solo_Track;
    public string Track_Designer;
    // public GameControl ctrl/* = null*/;
    

    enum MenuState
    {
        DesignEditorA,
        DesignEditorB,
    }

    // Use this for initialization
    void Start()
    {
        menu.Add(MenuState.DesignEditorA, new Action(DesignMenu1));
        menu.Add(MenuState.DesignEditorB, new Action(DesignMenu2));

    }
    void DesignMenu1()
    {
        GameObject.Find("MenuImage").GetComponent<Image>().sprite = source.DesignMenu1;
        if (Input.GetKeyDown(KeyCode.Return));
        {
            SceneManager.LoadScene(Track_Designer);
        }

    }
    void DesignMenu2()
    {
        GameObject.Find("MenuImage").GetComponent<Image>().sprite = source.DesignMenu1;
        if (Input.GetKeyDown(KeyCode.Return));
        {
            SceneManager.LoadScene(Track_Designer);
        }

    }



}
