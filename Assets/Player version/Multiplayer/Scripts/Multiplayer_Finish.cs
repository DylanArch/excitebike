﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class Multiplayer_Finish : MonoBehaviour
{

    //Code by: Levi Roberts
    //Have Questions? Contact me @ LJRoberts@student.fullsail.edu 
    //or on campus any time at fullsail


    public Animator BikeAnim; //Need this to do sick wheelies!
    public Motorcycle_Movement MotoBike; //Grabbing the script!
    public Multiplayer_Timer Timer; //Grabbing the script!
    public Animator HUDAnim;

    void Start() //Empty incase needed.
    {
        
    }

    void OnTriggerEnter(Collider col) //Collision makes me do things.
    {
        if (col.gameObject.name == "PlayerCharacter") //If player character collides with me imma make em do a sick wheelie!
        {
           BikeAnim.SetTrigger("isRaceOver?"); //Does a sick wheelie!
            MotoBike.enabled = false;  //Disables Motorscyle_Movement script on PlayerCharacter!
            /*Timer.enabled = false;*/     //Disables the Multiplayer_Timer script on UI_HUD_Timers!
            HUDAnim.SetBool("isBoosting?", false);
            Timer.finished = true;


        }

       SceneManager.LoadScene("WhateverLevelIsNext"); //Loads the next level.

        
    }

}
