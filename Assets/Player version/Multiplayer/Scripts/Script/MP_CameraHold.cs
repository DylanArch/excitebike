﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MP_CameraHold : MonoBehaviour {

    //Name: Mason Pickle
    //Date: 1/24/2018
    //Purpose: To keep the camera steady and following the floor's y axis as the player moves. 

    Vector3 Pos;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Pos = new Vector3(GameObject.Find("PlayerContainer").transform.position.x, 0.59f + GameObject.Find("floor").transform.position.y, -10);
        transform.SetPositionAndRotation(Pos, transform.rotation);
	}
}
