﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mudslide : MonoBehaviour {

    //Name: Leon Persson Mijac
    //Date: 1/24/2018
    //Purpose: Mudslide prefab slowing down speed of player bike

    MP_Boost_Accel speed;


    // Use this for initialization
    void Start()
    {
        speed = GameObject.Find("Player").GetComponent<MP_Boost_Accel>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //SceneManager.LoadScene("Menu");
            Debug.Log("TEst");
            speed.ResetSpeed();

        }

    }
}