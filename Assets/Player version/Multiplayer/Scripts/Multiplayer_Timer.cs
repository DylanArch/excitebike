﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Multiplayer_Timer : MonoBehaviour
{


    //Code by: Levi Roberts
    //Have Questions? Contact me @ LJRoberts@student.fullsail.edu 
    //or on campus any time at fullsail

    public Text timerText;
    public Text highscore;
    private float startTime;
    public bool finished = false;
    public float Highscore;
    public static Multiplayer_Timer timer;



    void Start()
    {
        //int t = 0;
        startTime   = Time.time;
        //highscore.text = PlayerPrefs.GetInt("HighScore", 1).ToString();
        //string minutes = Mathf.Floor((int)t / 60).ToString();
        //string seconds = Mathf.Floor(t % 60).ToString();
        //string fraction = Mathf.Floor((t * 100) % 100).ToString();
        highscore.text = PlayerPrefs.GetInt("Highscore",0).ToString();

    }



        void OnApplicationQuit()
        {
            PlayerPrefs.Save();
            print("Running");
        }


    void GameFinished()
    {
        float t = Time.time - startTime;
        if (t < PlayerPrefs.GetInt("HighScore", 0))
        {
            PlayerPrefs.SetInt("HighScore",0);
            highscore.text = PlayerPrefs.GetInt("Highscore", 0).ToString();


        }



    }
    void Update()
    {

        float t = Time.time - startTime;
        string minutes = Mathf.Floor((int)t / 60).ToString();
        string seconds = Mathf.Floor(t % 60).ToString();
        string fraction = Mathf.Floor((t * 100) % 100).ToString();
        timerText.text = string.Format("{0:0} : {1:00} : {2:000}", minutes, seconds, fraction);

        if (finished == true)
        {
            GameFinished();
           
        }

    }
   
}
