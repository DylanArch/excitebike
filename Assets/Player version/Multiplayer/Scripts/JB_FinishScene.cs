﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class JB_FinishScene : MonoBehaviour
    // Code By Joshua Barber
{
    
    void OnTriggerEnter(Collider other)
    {
        SceneManager.LoadScene("JB_Finish");
    }
}