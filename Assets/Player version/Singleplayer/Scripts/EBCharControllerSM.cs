﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


/*        

//        Developer Name: Brian Fitzgerald

//         Contribution: Levi Roberts (Motorcycle_Movement Script) - Translation code for actualy moving the bike forward, up, and down. ByTheTale (StateMachine) Provided by Shawn Kendall

//                Character Controller State Machine

//                Start: 3/13/2018 & End: 3/16/18

//                References: Motorcycle_Movement.cs

//                Links: https://assetstore.unity.com/packages/tools/c-state-machine-66224 - State Machine 

//*/

public class EBCharControllerSM : ByTheTale.StateMachine.MachineBehaviour {

    public override void AddStates() //Adding all the states to the state machine
    {
        AddState<IdleState>(); //Not moving
        AddState<DriveState>(); //Normal Speed Movement
        AddState<BoostState>(); //Boosted Movement
        AddState<OverHeatState>(); //Overheated
        AddState<CrashState>(); //Crashing. I will have the functionality inside crashing, but not the entrance into the state. This will be ready whenever crashing is working.

        SetInitialState<IdleState>(); //Game starts in Idle
    }

}

public class EBState : ByTheTale.StateMachine.State //Base state for the bike. Pulls in the Bike and Data variables at the beginning of the state.
{

    protected Transform Bike; //declare variables to be used in Enter
    protected EBCharControllerData Data; //^^

    public override void Enter()
    {
        base.Enter();
        Bike = GetMachine<EBCharControllerSM>().GetComponent<Transform>(); //The GameObject of the bike, used to control the bikes movement
        Data = GetMachine<EBCharControllerSM>().GetComponent<EBCharControllerData>(); //This script holds all the data values for the bike, such as heat and speed
    }

}

public class DriveState : EBState //Drive State, inherits from EBState
{

    public override void Enter() //called once when entering state
    {
        base.Enter(); //calls the Enter function from EBState, assigning the variables needed.
        Data.Move(); //Sets the speed to normal speed
    }

    public override void Execute() //runs on update
    {
        if(Data.GetHeat() < 50) //If the bike is not hot enough
        {
            Data.HeatUp(); //Heat it up.
        }
        if (Data.GetHeat() > 50) //if the bike is too hot
        {
            Data.CoolDown(); //Cool down the bike some
        }

        Bike.Translate(Vector3.right * Data.GetSpeed()); // moves the bike to the right using speed

        if (Input.GetKeyUp(KeyCode.D)) //If the player stops pressing d
        {
            machine.ChangeState<IdleState>(); //Go to the idle state
        }

        if (Input.GetKeyDown(KeyCode.Space)) // if the player presses space
        {
            machine.ChangeState<BoostState>(); //BOOOOOST
        }

        if (Input.GetKeyDown(KeyCode.W)) //Hit W to change lane up.
        {
            Bike.transform.Translate(0, 50, 0); //Make it go 50 units on the Y axis.
        }
        if (Input.GetKeyDown(KeyCode.S)) //Hit S to change lane down.
        {
            Bike.transform.Translate(0, -50, 0); //Make it go -50 units on the Y axis.
        }
    }

    public override void Exit() //Called once on leaving the state
    {
        Data.ResetSpeed(); //Set the speed back to 0
    }
}

public class BoostState : EBState //BOOOOST, inherits from EBState
{

    public override void Enter() //called once on entering the state
    {
        base.Enter(); //calls the Enter function from EBState, assigning the variables needed.
        Data.Boost(); //set the speed to the boost value
    }

    public override void Execute() //runs on update
    {
   
        Data.HeatUp(); //increment the heat value in the data script
        Bike.Translate(Vector3.right * Data.GetSpeed()); //move the bike to the right using the speed value from the data script
        if (Input.GetKeyUp(KeyCode.Space)) //if they stop pressing space
        {

            if (Input.GetKey(KeyCode.D))//if theyre pressing D
            {
                machine.ChangeState<DriveState>(); //Go to Drive
            }
            else
            {
                machine.ChangeState<IdleState>();//go to idle
            }
        }

        if (Data.Overheated()) //If heat gets higher than max heat
        {
            machine.ChangeState<OverHeatState>(); //Go to overheat
        }

        if (Input.GetKeyDown(KeyCode.W)) //Hit W to change lane up.
        {
            Bike.transform.Translate(0, 50, 0); //Make it go 50 units on the Y axis.
        }
        if (Input.GetKeyDown(KeyCode.S)) //Hit S to change lane down.
        {
            Bike.transform.Translate(0, -50, 0); //Make it go -50 units on the Y axis.
        }
    }

    public override void Exit() //called once upon leaving the state
    {
        Data.ResetSpeed(); //resets speed to 0
    }
}

public class IdleState : EBState //Idle state, when the bike is not moving
{

    public override void Enter() //called once upon entering the state
    {
        base.Enter(); //assigns the values from the data script
        Data.ResetSpeed(); // sets speed to 0
    }

    public override void Execute()
    {
        if(Data.GetHeat() > 0) //if the bike is hot
        {
            Data.CoolDown(); //Cool down the bike
        }
        if (Input.GetKeyDown(KeyCode.D)) //if the player presses D
        {
            machine.ChangeState<DriveState>();//go to Drive
        }
        if (Input.GetKeyDown(KeyCode.Space)) // if the player presses space
        {
            machine.ChangeState<BoostState>(); //BOOOOOST
        }
    }

    public override void Exit() // called once upon leaving the state
    {
        Data.ResetSpeed(); //set speed to 0;
    }
}
public class OverHeatState : EBState //Overheat state, player cannot move for 3 seconds
{
    public override void Enter() //called once upon entering state
    {
        base.Enter(); //sets values from data script
        Data.ResetSpeed(); //sets speed to 0
        Data.OverHeat();//calls the overheat coroutine
    }



    public override void Exit() // called once upon leaving the state
    {
        Data.ResetSpeed(); //set speed to 0
        Data.ResetHeat(); //Reset Heat to 0
    }
}

public class CrashState : EBState //The crash state. Currently there is no way to enter this state, this will need to be added to other states when this feature is implemented.
{
    public override void Enter() //Called once upon entering the state
    {
        
        base.Enter(); //sets values from Data script
        Data.ResetSpeed(); //sets speed to 0
        Data.Crash();
    }

    public override void Exit() //Called once upon leaving state
    {
        Data.ResetSpeed(); //Set speed to 0
    }
}
