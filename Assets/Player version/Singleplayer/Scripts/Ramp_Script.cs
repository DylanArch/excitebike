﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ramp_Script : MonoBehaviour
{
    //Code by: Levi Roberts
    //Have Questions? Contact me @ LJRoberts@student.fullsail.edu 
    //or on campus any time at fullsail

    public Animator Bikeanim;


    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "PlayerCharacter")
        {
            Bikeanim.SetTrigger("isJump?");
        }
           
    }
}