﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motorcycle_Movement : MonoBehaviour {
   
    
    
    //Code by: Levi Roberts
    //Have Questions? Contact me @ LJRoberts@student.fullsail.edu 
    //or on campus any time at fullsail



    public Rigidbody MotoRB;  //Character's Rigidbody, but you already know that!
    public GameObject MotoBike; //Character's GameObject, but you already know that!
    public float Speed = 0; //Character's Speed, but you already know that!
    public Animator Animator;//character's Animator, but you already know that!
    public float SpeedBoost = 0;
    public bool canBoost;
    public Animator HUD;





    // Use this for initialization, skrrt!
    void Start() //Doing things on startup
    {
        MotoRB = GetComponent<Rigidbody>(); //MotoRB is now grabbing the rigidbody and can be accessed by writing MotoRB
    }
	
	// Update is called once per frame, really fast skrrt!
	void Update () //Do things every frame.
    {
		if (Input.GetKey(KeyCode.D)) //Hit D to go Right. also animates now!!!
        {

            transform.Translate(Vector3.right * Speed); //Makes it go 1 unit on the X axis times the variable called "Speed"
            Animator.SetBool("isMoving?", true); //Sets the bool to true in the animator.

        }
        
        if (Input.GetKeyUp(KeyCode.D)) //Resets the bool to false and puts it in idle state.
        {
            Animator.SetBool("isMoving?", false); //Sets the bool to false in the animator.
        }

        if (Input.GetKeyDown(KeyCode.W)) //Hit W to change lane up. Animation soon TM.
        {

            transform.Translate(0, 50, 0); //Make it go 50 units on the Y axis.
            Animator.SetTrigger("isLaneChange?");

        }
        if (Input.GetKeyDown(KeyCode.S)) //Hit S to change lane down. Animation soon TM.
        {

            transform.Translate(0, -50, 0); //Make it go -50 units on the Y axis.
            Animator.SetTrigger("isLaneChangeDown?");

        }

        if (Input.GetKey(KeyCode.Space) && canBoost) //Hit Spacebar to do a sick wheelie!
        {
            
            transform.Translate(Vector3.right * SpeedBoost); //adds speed for holding spacebar.
            HUD.SetBool("isBoosting?",true);
           new  WaitForSeconds(5);
            canBoost = false;
            

        }
        else
        {
            HUD.SetBool("isCooldown?",true);
            HUD.SetBool("isBoosting?", false);
            new WaitForSeconds(1);
            canBoost = true;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Animator.SetTrigger("isRaceOver?"); //Set the trigger to activate.
        }
           
	}
}
