﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*        

//        Developer Name: Brian Fitzgerald

//         Contribution: 

//                Character Controller Data File

//                Start: 3/13/2018 & End: 3/15/2018

//                References: GameDevelopement with Kendall

//                Links:

//*/

public class EBCharControllerData : MonoBehaviour {

    [SerializeField] private float MBSpeed; //Current Speed
    [SerializeField] private float MBHeat; //Current Heat
    [SerializeField] private float NormalSpeed; //Default speed for Drive
    [SerializeField] private float BoostSpeed; //Default speed for Boost
    [SerializeField] private float MaxHeat; //Overheat Limit
    [SerializeField] private EBCharControllerSM StateMachine; //The state machine
    [SerializeField] private float HeatDec;

	// Use this for initialization
	void Start () { //set all the default values

        NormalSpeed = 10.0f; 
        BoostSpeed = 20.0f;
        MBSpeed = 0.0f;
        MBHeat = 0.0f;
        MaxHeat = 100.0f;

	}


    public void Boost() 
    {
        MBSpeed = BoostSpeed; //set current speed to boost speed
    }

    public void Move()
    {
        MBSpeed = NormalSpeed; //set current speed to normal speed
    }

    public void ResetSpeed()
    {
        MBSpeed = 0.0f; //set current speed to 0
    }

    public float GetHeat()
    {
        return MBHeat; //Returns the current heat value
    }

    public void HeatUp()
    {
        MBHeat+= 0.5f; //increment heat value
    }

    public void CoolDown()
    {
        MBHeat-= 0.5f; //Decrement heat value
    }

    public bool Overheated()
    {
        return (MBHeat > MaxHeat); //is the bike overheated?
    }

    public float GetSpeed()
    {
        return MBSpeed; //check the current speed
    }

    public void ResetHeat()
    {
        MBHeat = 0; //Reset the heat to 0
    }

    public void OverHeat() //if they overheat
    {
        StartCoroutine(Stop()); //they get stopped
    }

    public void Crash() //if they crash
    {
        StartCoroutine(Stop()); //they get stopped
    }
    IEnumerator Stop()
    { //when they call Stop

        
        yield return new WaitForSeconds(3); //wait for 3 seconds
        StateMachine.ChangeState<IdleState>(); //then go to idle
    }
    public void BoostArrow() //When we go over a boost arrow
    {
        MBHeat = MBHeat - HeatDec; //decrease heat
        if(MBHeat < 0) //if the heat goes below 0
        {
            MBHeat = 0; //it doesn't go below 0
        }
    }
    public float HeatRatio()
    {
        return MBHeat / MaxHeat;
    }
}
