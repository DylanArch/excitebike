﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;



//        Developer Name: James Hollingsworth

//         Contribution: Save and load data now available. need to add the array for gameObjects to the game.

//                Feature: Both Save and Load are featured here

//                Start & End dates : 03/06/2018

//                References: https://unity3d.com/learn/tutorials/topics/scripting/persistence-saving-and-loading-data

//                Used this video to do the basic binary save and load feature. Do to the prefab layout of the array, I was unable to add them to the save currently.
public class GameControl : MonoBehaviour
{

    // Example of save data. I could not get the array to properly become a variable.
    public float health;
    public float experience;



    // How the Script works. It is on the "Save" game prefab.
    public static GameControl control;

    void Awake()
    {
        if (control == null)
        {
            DontDestroyOnLoad(gameObject);
            control = this;
        }
        else if (control != this)
        {
            Destroy(gameObject);
        }

    }


    // To show the data of change for example.
    void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 100, 30), "Health: " + health);
        GUI.Label(new Rect(10, 40, 150, 30), "Experience: " + experience);

    }

    // The binary save feature completed.Replace data after LevelSlot data = new LevelSlot(); for updates.
    public void Save()
    {
        Input.GetKeyDown(KeyCode.Alpha1);

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");
        LevelSlot data = new LevelSlot();
        data.health = health;
        data.experience = experience;
        bf.Serialize(file, data);
        file.Close();
    }


    // The binary load feature completed. Replace data after file.Close(); for updates.
    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            Input.GetKeyDown(KeyCode.Alpha2);
            print("Loading");
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            LevelSlot data = (LevelSlot)bf.Deserialize(file);
            file.Close();
            health = data.health;
            experience = data.experience;
        }
    }
}


// This is just example data for showing save. Update with array's when completed.
 [Serializable]

    class LevelSlot
    {
        // public MenuController totallevels;
        // public SaveLoadGame Saving;
        // public SaveLoadGame Loading;
        public float health;
        public float experience;

    }
