﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController1: MonoBehaviour
{

    int index = 0;
    public int totalLevels = 21;
    public float yOffset;


    // Use this for initialization
    void Start()
    { 

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (index < totalLevels - 1)
            {
                index++;
                //Vector2 position = transform.position;
                //position.x += yOffset;
                transform.position = new Vector3(transform.position.x + yOffset, transform.position.y, transform.position.z);
            }
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (index > 0)
            {
                index--;
                //Vector2 position = transform.position;
                //position.x -= yOffset;
                transform.position = new Vector3(transform.position.x - yOffset, transform.position.y, transform.position.z);
            }

        }
    }
}

