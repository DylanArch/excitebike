﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




// Original developer unknown edited by James Hollingsworth
// Contribution: Rework of keys to allow placement of index
// Feature: Track Designer Controls
public class MenuController : MonoBehaviour
{

    int index = 0;
    //Done by original scripter. I do think the public int totalLevels is being called from Button_Prefabs.
    public int totalLevels = 21;
    //Done by original scripter. I believe yOffset is used to generate the transform GameObjects.
    public float yOffset;



    // Update is called once per frame
    void Update()
    {
        //Move right on the letter prefabs.
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (index < totalLevels - 1)
            {
                index++;
                //Vector2 position = transform.position;
                //position.x += yOffset;
            }
            //else (Input.GetKeyDown(KeyCode.Return)){
            //    transform.position = new Vector3(transform.position.x + yOffset, transform.position.y, transform.position.z);
            //}
        }
        //Move left on letter prefabs.
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (index > 0)
            {
                index--;
                //Vector2 position = transform.position;
                //position.x -= yOffset;
                transform.position = new Vector3(transform.position.x - yOffset, transform.position.y, transform.position.z);
            }

        }
    }
}

