﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//        Developer Name: Original unknown. Edited by James Hollingsworth

//         Contribution: Combined MenuController and ButtonPrefab. Attempted to create input for return to place items, Unable to scoll with using
//          left and right.

//                Feature: Track Designer Controls update

//                Start & End dates: 03/11/2018 to 03/16/2018

//                References: Need to create foreach to instantiate objects.

//                        Links: https://answers.unity.com/questions/302761/understanding-a-foreach-loop.html


public class Button_Prefabs_Loader : MonoBehaviour
{
    // GameObjects for one Array. Possible array for save feature, currently do not have this linked to the save in GameControl.
    public GameObject A;
    public GameObject B;
    public GameObject C;
    public GameObject D;
    public GameObject E;
    public GameObject F;
    public GameObject G;
    public GameObject H;
    public GameObject I;
    public GameObject J;
    public GameObject K;
    public GameObject L;
    public GameObject M;
    public GameObject N;
    public GameObject O;
    public GameObject P;
    public GameObject Q;
    public GameObject R;
    public GameObject S;



    public GameObject Obj2prnt;

    int index = 0;
    public int totalLevels = 21;
    public float yOffset;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.R))
        {
            if (index < totalLevels - 1)
            {
                index++;
                //Vector2 position = transform.position;
                //position.x += yOffset;
            
            //if (Input.GetKeyDown(KeyCode.E))
            
                transform.position = new Vector3(transform.position.x + yOffset, transform.position.y, transform.position.z);
            }
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            if (index > 0)
            {
                index--;
                //Vector2 position = transform.position;
                //position.x -= yOffset;
                transform.position = new Vector3(transform.position.x - yOffset, transform.position.y, transform.position.z);
            }

        }
    }




    //Need to fix from Instantiate. Should require KeyCode.Return to Instantiate.
    void OnTriggerEnter(Collider col)
    {

        if (col.gameObject.name == "a") // && Input.GetKeyDown(KeyCode.Return))
        {
            Instantiate(A, Obj2prnt.transform.position, col.transform.rotation);

        }


        if (col.gameObject.name == "b")
        {
            Instantiate(B, Obj2prnt.transform.position, col.transform.rotation);


        }
        if (col.gameObject.name == "c")
        {
            Instantiate(C, Obj2prnt.transform.position, col.transform.rotation);

        }
        if (col.gameObject.name == "d")
        {
            Instantiate(D, Obj2prnt.transform.position, col.transform.rotation);

        }
        if (col.gameObject.name == "e")
        {
            Instantiate(E, Obj2prnt.transform.position, col.transform.rotation);

        }

        if (col.gameObject.name == "f")
        {
            Instantiate(F, Obj2prnt.transform.position, col.transform.rotation);

        }
        if (col.gameObject.name == "g")
        {
            Instantiate(G, Obj2prnt.transform.position, col.transform.rotation);

        }
        if (col.gameObject.name == "h")
        {
            Instantiate(H, Obj2prnt.transform.position, col.transform.rotation);

        }
        if (col.gameObject.name == "i")
        {
            Instantiate(I, Obj2prnt.transform.position, col.transform.rotation);

        }
        if (col.gameObject.name == "j")
        {
            Instantiate(J, Obj2prnt.transform.position, col.transform.rotation);

        }
        if (col.gameObject.name == "k")
        {
            Instantiate(K, Obj2prnt.transform.position, col.transform.rotation);

        }
        if (col.gameObject.name == "l")
        {
            Instantiate(L, Obj2prnt.transform.position, col.transform.rotation);

        }
        if (col.gameObject.name == "m")
        {
            Instantiate(M, Obj2prnt.transform.position, col.transform.rotation);

        }
        if (col.gameObject.name == "n")
        {
            Instantiate(N, Obj2prnt.transform.position, col.transform.rotation);

        }
        if (col.gameObject.name == "o")
        {
            Instantiate(O, Obj2prnt.transform.position, col.transform.rotation);

        }
        if (col.gameObject.name == "p")
        {
            Instantiate(P, Obj2prnt.transform.position, col.transform.rotation);

        }
        if (col.gameObject.name == "q")
        {
            Instantiate(Q, Obj2prnt.transform.position, col.transform.rotation);

        }
        if (col.gameObject.name == "r")
        {
            Instantiate(R, Obj2prnt.transform.position, col.transform.rotation);

        }
        if (col.gameObject.name == "s")
        {
            Instantiate(S, Obj2prnt.transform.position, col.transform.rotation);

        }
    }
}
