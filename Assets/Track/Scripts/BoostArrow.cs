﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*        

//        Developer Name: Brian Fitzgerald

//         Contribution: 

//                Boost Arrow Trigger Script

//                Start: 3/15/2018 & End: 3/15/2018

//                References:

//                Links:

//*/

public class BoostArrow : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other) //When something hits us
    {
        if(other.tag == "Bike") //if its a bike
        {
            other.GetComponent<EBCharControllerData>().BoostArrow(); //call its boost arrow function
        }
    }
}
