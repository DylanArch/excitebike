﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*        

//        Developer Name: Brian Fitzgerald

//         Contribution: 

//                Heat Bar Script

//                Start: 3/15/2018 & End: 3/15/2018

//                References:

//                Links:

//*/

public class HeatBar : MonoBehaviour {

    [SerializeField] EBCharControllerData Data;
    [SerializeField]
    private float HR;
    public Vector3 PosEmpty;
    public Vector3 ScaleEmpty;
    public Vector3 PosFull;
    public Vector3 ScaleFull;
    

	// Use this for initialization
	void Start () {

        PosEmpty = new Vector3(-0.4659f, .11f, -6.1f);
        ScaleEmpty = new Vector3(0.0f, 0.5f, 14.9684f);
        PosFull = new Vector3(0.0229f, 0.11f, -6.1f);
        ScaleFull = new Vector3(1.004f, 0.5f, 14.9684f);
        gameObject.transform.localScale = ScaleEmpty;
        gameObject.transform.localPosition = PosEmpty;

	}
	
	// Update is called once per frame
	void Update () {
        HR = Data.HeatRatio();
        gameObject.transform.localScale = Vector3.Lerp(ScaleEmpty, ScaleFull, HR);
        gameObject.transform.localPosition = Vector3.Lerp(PosEmpty, PosFull, HR);
	}
}
