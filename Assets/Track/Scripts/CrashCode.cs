﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//by: David Pearce (Start: 3/12/18)
//have Questions? Contact me @DAPearce@student.fullsail.edu
//Edit: Added Trigger Code for Oil Spill Asset to slow player.(3/14/18)
//Edit: refurbished Code to utilize a public Speed Float with trigger and collision lines for future assets.(3/14/18)
//Finished Collision Code 3/15/18 Script may be added to any collision based asset affecting speed.

public class CrashCode : MonoBehaviour {
	public GameObject MotoBike;
	private float TimeElapsed = 3;
	private bool StartTime = false;
	private Motorcycle_Movement PMScript;
	public float CollisionSpeed;

	void Start ()
	{
		PMScript = MotoBike.GetComponent<Motorcycle_Movement> ();
	}


	void Update (){
		if (StartTime == true) 
		{
			TimeElapsed -= 1*Time.deltaTime;

			if (TimeElapsed <= 0) {
				PMScript.Speed = 10;
				TimeElapsed = 3;
				StartTime = false;
			}
		}
		
	}

	public float GetTime(){
		return TimeElapsed;
	}
	public void ResetTimeSinceLastRestart()
	{
		TimeElapsed = 0;
	}

	void OnCollisionEnter (Collision col)
	{
		if(col.gameObject.tag == "Player") 
			
		{
			PMScript.Speed = CollisionSpeed;
			StartTime = true;
		}
	
	}

	void OnTriggerEnter (Collider Col)
	{
		if(Col.gameObject.tag == "Player") 

		{
			PMScript.Speed = CollisionSpeed;
			StartTime = true;
		}
	}
}